# CesiumJS Workshop

## 1 Introduction

CesiumJS est une bibliothèque JavaScript open-source permettant de créer des globes et des cartes en 3D dans les navigateurs web. Elle fournit aux développeurs les outils nécessaires pour créer des applications géospatiales interactives et performantes qui peuvent afficher des données géographiques en 3D ou en 2D, avec la prise en charge de diverses projections cartographiques, de la visualisation du terrain, de couches d'imagerie et de modèles 3D. CesiumJS s'appuie sur WebGL, une technologie graphique qui permet d'accélérer matériellement le rendu des graphiques 3D dans les navigateurs web, ce qui permet de créer des applications géospatiales sophistiquées et visuellement attrayantes qui s'exécutent dans les navigateurs web modernes sans nécessiter de plugins ou d'autres logiciels tiers.

CesiumJS offre un large éventail de fonctionnalités, notamment la prise en charge de la visualisation de données géographiques, de données de terrain et d'élévation, de couches d'imagerie provenant de diverses sources, de modèles 3D et de données vectorielles. Il offre de puissantes possibilités d'ajout d'interactivité, telles que des interactions avec l'utilisateur comme le panoramique, le zoom et la rotation du globe, ainsi que la gestion des événements de la souris, des contrôles de caméra et de l'animation. CesiumJS prend également en charge l'intégration avec d'autres bibliothèques et services géospatiaux populaires, tels que OpenStreetMap, Bing Maps, Google Earth, etc.

CesiumJS est largement utilisé dans une variété d'applications, y compris la visualisation géospatiale pour l'observation de la terre, le suivi des satellites, la planification urbaine, la simulation de vol, les globes virtuels, et de nombreux autres cas d'utilisation où la visualisation géospatiale interactive en 3D est nécessaire. Il est activement entretenu et soutenu par une communauté dynamique de développeurs, et il est disponible sous une licence open-source, ce qui signifie qu'il peut être utilisé, modifié et distribué librement.

Cesium JS est développé par l'entreprise américaine AGI (Ansys Government Initiatives). Le concept commercial d'AGI est d'offrir un client open-source (Cesium JS) et des services (payantes) qui permettent de facilement inclure des données dans Cesium JS (modèle numérique de terrain p.ex) et de faire du hosting de ses propres données (Cesium ION).

La référence de Cesium se trouve ici : https://cesium.com/learn/cesiumjs/ref-doc/

## 2 Hello World

Ouvrir ex.html

(si le token de Cesium ION ne fonctionne pas -> il faut générer un nouveau token)
-> Cesium est une librairie Javascript qu'on peut inclure dans une page HTML (balises script)

L'élément principal de CesiumJS s'appelle "viewer"; on peut y configurer :
- les éléments graphiques de l'interface ("widgets")
- les données input

Quelques éléments graphiques configurables sont :

### baseLayerPicker
Le BaseLayerPicker est un widget à bouton unique qui affiche un panneau de fournisseurs d'images et de terrains disponibles. Lorsque l'imagerie est sélectionnée, la couche d'imagerie correspondante est créée et insérée en tant que couche de base de la collection d'imagerie, en supprimant la base existante. Lorsque le terrain est sélectionné, il remplace le fournisseur de terrain actuel. Chaque élément de la liste des fournisseurs disponibles contient un nom, une icône représentative et une infobulle permettant d'afficher plus d'informations au survol. La liste est initialement vide et doit être configurée avant d'être utilisée, comme illustré dans l'exemple ci-dessous.

### geocoder
Un widget pour trouver des adresses et des points de repère, et faire voler la caméra jusqu'à eux. Le géocodage est effectué à l'aide de Cesium ION.

### homeButton
Un widget à bouton unique pour revenir à la vue de la caméra par défaut de la scène actuelle.

### infoBox
Un widget pour afficher des informations ou une description. (le 'I' des SIG)

### navigationHelpButton
Le NavigationHelpButton est un widget à bouton unique permettant d'afficher des instructions pour naviguer sur le globe à l'aide de la souris.

### projectionPicker
Le ProjectionPicker est un widget à bouton unique qui permet de passer d'une projection perspective à une projection orthographique.

### sceneModePicker
Le SceneModePicker est un widget à bouton unique permettant de passer d'un mode de scène à l'autre ; il est illustré à gauche dans son état développé. La commutation programmée des modes de scène sera automatiquement reflétée dans le widget tant que la scène spécifiée est utilisée pour effectuer le changement.

### timeline
La timeline est un widget qui permet d'afficher et de contrôler la durée de la scène en cours.

### Exercice		
L'exemple de base a tous ces éléments activés
-> desactivez ces éléments en les mettant sur "false"
-> il faut déclarer chaque élément à l'intérieur des {} (les options du viewer - p.ex timeline = false; etc)

## 3 Contrôle de la caméra
Cesium JS contient une méthode pour controller la caméra (un élément du viewer). 

Une méthode de la caméra est flyTo :
https://cesium.com/learn/cesiumjs/ref-doc/Camera.html#flyTo

Cette méthode offre la possibilité d'indiquer le lieu soit en donnant une position absolue de la caméra (coordonnées WGS84 + altitude absolue) et une orientation (heading, pitch et roll)

Un exemple d'un appel est donné :

    viewer.camera.flyTo({
      destination : Cesium.Cartesian3.fromDegrees("lng, lat, alt"),
      orientation : {
        heading : Cesium.Math.toRadians(angle),
        pitch : Cesium.Math.toRadians(angle),
      }
    });

### Exercice
-> Inclure ce code dans votre code (après la définition du viewer) et ajoutez les coordonnées pour afficher les bâtiments de HES-SO Provence
-> Cesium JS devrait montrer les bâtiments de HES-SO Provence


## 4 Ajouter des autres données de base
Cesium est aujourd'hui utilisé par Swisstopo pour la version 3D de map.geo.admin.ch. Swisstopo mettent à disposition comme services web :
- Les couches WMTS en WGS84 (orthophotos, cartes, etc)
- Le modèle numérique de terrain sous forme de Quantized Mesh
- Les Swissbuildings 3D sous forme de Cesium 3D Vector Tiles

Un développeur de Swisstopo a également crée une pipeline pour générer le quantized mesh avec Python :
https://github.com/loicgasser/quantized-mesh-tile

### Exercice
-> Effacez la ligne dans Cesium pour définir le token de Cesium ION -> on va uniquement utiliser les données de Swisstopo

Pour inclure les données WMTS de Swisstopo utilisez un nouveau imageryProvider à l'intérieur de la configuration du viewer :

	  imageryProvider: new Cesium.UrlTemplateImageryProvider({
	    url:
	      "https://wmts{s}.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/{z}/{x}/{y}.jpeg",
	    subdomains: "56789",
	    availableLevels: [8, 10, 12, 14, 15, 16, 17, 18],
	    minimumRetrievingLevel: 8,
	    maximumLevel: 17,
	    tilingScheme: new Cesium.GeographicTilingScheme({
	      numberOfLevelZeroTilesX: 2,
	      numberOfLevelZeroTilesY: 1,
	    }),
	  }),

Pour inclure le quantized mesh de Swisstopo utilisez un nouveau terrainProvider à l'intérieur de la configuration du viewer :

	  terrainProvider: new Cesium.CesiumTerrainProvider({
	    url: "https://download.swissgeol.ch/cli_terrain/ch-2m/",
	    availableLevels: [
	      0,
	      1,
	      2,
	      3,
	      4,
	      5,
	      6,
	      7,
	      8,
	      9,
	      10,
	      11,
	      12,
	      13,
	      14,
	      15,
	      16,
	      18,
	      19,
	    ],
	    rectangle: Cesium.Rectangle.fromDegrees(7.13725, 47.83080, 10.44270, 45.88465), 
	  }),
	  
	  
-> Cesium devrait maintenant montrer la même scène, mais avec les données de Swisstopo


## 5 Inclure les bâtiments 3D

Swisstopo met à disposition les données des bâtiments 3D sous forme de Cesium 3D Tiles. Les 3D Tiles utilisent un schéma irrégulier de tuilage (contrairement à p.ex WMTS) -> il est ainsi nécessaire d'indiquer au client Cesium JS ou se trouve chaque tuile. Le mécanisme utilisé à cet effet est l'indication d'un fichier json qui contient le schéma de tuilage :
https://vectortiles0.geo.admin.ch/3d-tiles/ch.swisstopo.swisstlm3d.3d/20180716/tileset.json

Ce fichier contient des liens vers des autres fichiers json qui indiquent à Cesium quelles tuiles il faut charger quand on se déplace.

Pour inclure les bâtiments dans Cesium JS un mécanisme de "Overlay" est utilisé. Ce mécanisme n'est pas configuré à l'intérieur du viewer, mais appelé depuis l'extérieur à travers une fonction asynchrone.

### Exercice

Ajoutez le code suivant après la configuration du viewer pour afficher les bâtiments 3D de Swisstopo


	async function displayBuildings(){
		const buildings = await Cesium.Cesium3DTileset.fromUrl(
			"https://vectortiles0.geo.admin.ch/3d-tiles/ch.swisstopo.swisstlm3d.3d/20180716/tileset.json", {
			skipLevelOfDetail: true,
			baseScreenSpaceError: 1024,
			skipScreenSpaceErrorFactor: 16,
			skipLevels: 1,
			immediatelyLoadDesiredLevelOfDetail: false,
			loadSiblings: false,
			cullWithChildrenBounds: true
		});
		viewer.scene.primitives.add(buildings);
	}    

	displayBuildings();

## 6 Ajouter une couche KML
Cesium permet d'afficher des données temporelles, p.ex sous format KML avec une balise temporelle. Cette fonctionalité est p.ex intéressante si on veut visualiser des données de capteurs.

### Exercice
Ouvrez le fichier GSATu-1.kml du dossier data avec un éditeur texte.
-> KML permet uniquement des coordonnées en KML
-> KML permet de faire du styling

Le fichier GSATu-1.kml contient des données de capteurs de la patrouille des glaciers de 2008.

Cesium permet d'ajouter facilement des données KML avec Cesium.KmlDataSource

    const options = {
        camera: viewer.scene.camera,
        canvas: viewer.scene.canvas,
        screenOverlayContainer: viewer.container,
    };

    viewer.dataSources.add(
          Cesium.KmlDataSource.load(
            "mon_fichier_kml,
            options
          )
        );

-> Réactivez la timeline dans Cesium pour pouvoir naviguer au niveau temporel.
-> Changez la commande flyTo() pour aller dans la région de la patrouille des glaciers


## 7 Convertir un fichier IFC pour l'afficher dans Cesium

IFC est un format de fichier text. Il contient des différents éléments ("Class") qui définissent un bâtiment.

FME permet de lire et écrire des fichiers IFC à l'aide d'un IFC Reader/Writer.

### Exercice

Chargez le fichier IFC dans FME.
-> FME va créer des FeatureTypes par type de IFC class
-> Sélectionnez : IfcSlab (les étages) et IfcWallStandardCase (les murs)

Créez un writer "GLTF" dans FME
-> Choississez dans les paramêtres du GLTF writer "Single Binary File" : cela va créer un seul fichier binaire ".glb" par class
-> Branchez les deux class IfcSlab et IfcWallStandardCase aux deux writer feature types pour créer deux fichiers : IfcSlab.glb et IFCWallStandardCase.glb

Mettez ces deux fichiers dans le dossier data de votre dossier Cesium

Pour inclure un fichier glb dans Cesium il faut toujours définir la position exacte ou se trouvera le modèle et l'échelle.

Cesium connait une méthode pour charger un gLTF : fromGltf() - la syntaxe est la suivante :

    let scene = viewer.scene;
    let modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(lng, lat, alt));
    let IfcSlab = scene.primitives.add(Cesium.Model.fromGltf({
        url : 'mon_fichier_glb',
        modelMatrix : modelMatrix,
        scale : 1.0
	}));
	
-> modifiez le code pour afficher le bâtiment à un endroit choisi
